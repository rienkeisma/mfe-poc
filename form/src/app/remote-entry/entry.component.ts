import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NxWelcomeComponent } from './nx-welcome.component';

@Component({
  standalone: true,
  imports: [CommonModule, NxWelcomeComponent],
  selector: 'mfe-form-entry',
  template: `<mfe-nx-welcome></mfe-nx-welcome>`,
})
export class RemoteEntryComponent {}
