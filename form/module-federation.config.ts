import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'form',
  exposes: {
    './Routes': 'form/src/app/remote-entry/entry.routes.ts',
  },
};

export default config;
