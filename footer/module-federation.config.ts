import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'footer',
  exposes: {
    './Component': 'footer/src/app/remote-entry/entry.component.ts',
  },
};

export default config;
