import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'header',
  exposes: {
    './Component': 'header/src/app/remote-entry/entry.component.ts',
  },
};

export default config;
