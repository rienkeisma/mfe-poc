import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NxWelcomeComponent } from './nx-welcome.component';

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, RouterModule],
  selector: 'mfe-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'shell';
  @ViewChild('headerPlaceHolder', { read: ViewContainerRef })
  headerViewContainer!: ViewContainerRef;
  @ViewChild('footerPlaceHolder', { read: ViewContainerRef })
  footerViewContainer!: ViewContainerRef;
  ngOnInit(): void {
    this.loadRemotes();
  }
  async loadRemotes(): Promise<void> {
    const mh = await import('header/Component');
    const mf = await import('footer/Component');
    this.headerViewContainer.createComponent(mh.RemoteEntryComponent);
    this.footerViewContainer.createComponent(mf.RemoteEntryComponent);
  }
}
